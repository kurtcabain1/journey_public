# Journey 

## An intelligent, recovery oriented journal app

### Journal features
- App opens in today's journal entry
- Swipe left/right to browse through journal entries
- Journal calendar view
- Push notification reminders on set times

### Journal entry editor features
- Additive journal, add snippets tagged with registering time
- Manual tagging for emotional content, rational content and proper self reflection (color schemes)
- Manual tagging for Ego States of Mind (child, adult and parent)
- Manually mark journal entry as complete
- Track sporting activities
- Give compliments for completing an entry, based on the entry's sentiment
- Completion tracker (timeline design)

### Pattern recognition
- Track patterns in emotional content over multiple days (emotional vocabulary)
- Track keywords for Gestalt awareness disturbances (projection, introjection, retroflection, egotism and confluence)
- Track keywords for Step 6 character defects
- Track keywords for common blocks (grandiosity, suspicion, denial etc.)
- Figure out how to recognize thought patterns like denial, fear warping, imposterism etc.
- Track influence of sporting activities on emotional wellbeing and thought patterns
- https://www.codeproject.com/Articles/5282687/AI-Chatbots-With-TensorFlow-js-Detecting-Emotion-i

### Step 4
- Resentment, fear and damage inventory
- Character defect database for resentments with dictionary definitions

### Step 6
- Auto generate character defect list (scrape journal entries for keywords)

### Step 10 
- Include Step 10 questions in journal entry
- Link Step 10 resentments to registered Step 4 entries, search by person or keywords
- Link Step 10 fears to registered Step 4 entries

### Samsung Health connection (requires native app to push data to back-end server)
- Include Samsung Health data, relate to time tags if possible (e.g. 10.00 -> medium stress, heartrate 80, I was feeling a bit anxious and frustrated researching a hard-to-fix issue at work)
- Actively track stress meter - notification for pre-tagged journal entry for high stress moments
- Auto update step tracker